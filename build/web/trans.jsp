<!DOCTYPE html>
<html lang="en">
<title>W3.CSS Template</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    input[type=text], select, textarea {
  width: 50%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}

input[type=submit] {
  background-color: #4CAF50;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.container {
    border: 5px solid black;
    width:400px;
  background-color: #f2f2f2;

}
body{
    background-color: black;
}
body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
.w3-bar,h1,button {font-family: "Montserrat", sans-serif}
.fa-anchor,.fa-coffee {font-size:200px}
</style>
<body>
    <%
        String name=String.valueOf(session.getAttribute("name"));
        %>
<!-- Navbar -->
<div class="w3-top">
  <div class="w3-bar w3-red w3-card w3-left-align w3-large">
    <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-red" href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
    <a href="home.jsp" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">Home</a>
    <a href="check.jsp" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">check balance</a>
    <a href="trans.jsp" class="w3-bar-item w3-button w3-padding-large w3-white">transfer</a>
    <a href="view.jsp" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">view all</a>
    
  </div>

  
</div>

<!-- Header -->
<h3>Contact Form</h3>
<center>
<div class="container">
  <form action="trans1.jsp">
      <h1> Money transfer</h1>
    <label for="fname">To &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</label>
    <input type="text" id="fname" name="to" placeholder="enter sender username"><br>

    <label for="lname">amount</label>
    <input type="text" id="lname" name="amount" placeholder="Your last name..">

  


     &nbsp; &nbsp;<textarea id="subject" name="subject" placeholder="Write something.." style="height:200px"></textarea>
   <br>
    <input type="submit" value="send">
  </form>
</div>

</header>

</center>

<script>
// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
  var x = document.getElementById("navDemo");
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}
</script>

</body>
</html>
