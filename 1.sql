/*
SQLyog - Free MySQL GUI v5.13
Host - 5.0.22-community-nt : Database - money
*********************************************************************
Server version : 5.0.22-community-nt
*/

SET NAMES utf8;

SET SQL_MODE='';

create database if not exists `money`;

USE `money`;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';

/*Table structure for table `members` */

DROP TABLE IF EXISTS `members`;

CREATE TABLE `members` (
  `username` varchar(50) default NULL,
  `email` varchar(255) default NULL,
  `password` varchar(255) default NULL,
  `mobile` varchar(255) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `members` */

insert into `members` (`username`,`email`,`password`,`mobile`) values ('gokul','gokul@gmail.com','asdf','9789639635'),('mahilan','mahilan@gmail.com','asdf','58858545');

SET SQL_MODE=@OLD_SQL_MODE;